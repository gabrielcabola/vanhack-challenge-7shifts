<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>7 Shifts Challenge - Detail of a Location {{ $location->address }}</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">
      <link  href="{{ asset('css/app.css') }}" rel="stylesheet" type="text/css">

        <style>

        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Raleway', sans-serif;
                font-weight: 100;
                height: 100vh;
                margin: 0;
                padding: 20px;
            }
            ul { list-style: none; }
            ul > li {
              text-align: left;
              padding: 0;
               margin:0;
             }
            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 50px;
            }

            .links > a {
                color: orange;
                padding: 0 25px;
                font-size: 12px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .location {
              border: 1px solid #EAEAEA;
              padding:20px;
              border-radius: 3px;
            }
            .user { border-bottom: 1px solid #EAEAEA;}

            .btn { margin:10px 0px; }
            .m-b-md {
                margin-bottom: 30px;
            }
        </style>
    </head>
    <body>
        <div class="position-ref full-height" id="app" >


            <div class="content">

              <a href="/">Back to Locations</a>
                <div class="title m-b-md">

                  <strong>  Location Detail {{ $location->address }} - <small>{{ $location->city }} - {{ $location->country}}</small></strong>
                </div>

                <location-detail :users="{{ json_encode($users) }}" :location="{{ json_encode($location)}}"></location-detail>
              

            </div>
        </div>

    </body>
  <script src="{{ asset('js/app.js') }}"></script>


</html>
