<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>7 Shifts Challenge - Location List</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">

      <link  href="{{ asset('css/app.css') }}" rel="stylesheet" type="text/css">
        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Raleway', sans-serif;
                font-weight: 100;
                height: 100vh;
                margin: 0;
            }
            ul { list-style: none; }
            ul > li {
              text-align: left;
              padding: 0;
               margin:0;
             }
            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 50px;
            }

            .links > a {
                color: orange;
                padding: 0 25px;
                font-size: 12px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .location {
              border: 1px solid #EAEAEA;
              padding:20px;
              border-radius: 3px;
            }

            .btn { margin:10px 0px; }
            .m-b-md {
                margin-bottom: 30px;
            }
        </style>
    </head>
    <body>
        <div class="flex-center position-ref full-height">


            <div class="content">
                <div class="title m-b-md">
                    Locations
                </div>

                <ul>
                  @foreach ($locations as $key => $place)
                      <li class="location"><h2>{{ $place->address }}</h2>
                        <span>{{ $place->city }} - {{ $place->country}}</span><br>
                         <a class="btn btn-lg btn-primary" href="/location/{{ $place->id }}">See details</a>
                       </li>
                  @endforeach

                </ul>

            </div>
        </div>
    </body>
</html>
