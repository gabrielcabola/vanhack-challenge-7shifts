<?php

namespace App\Contracts\Repositories;

interface DataRepository
{
    /**
     * Get the Json and Convert in a Collection.
     *
     * @param  string|int  $endpoint
     * @return \Illuminate\Database\Eloquent\Collection
     */

    public function get($endpoint);



    /**
     * Convert the json into a collection
     *
     * @param  array $data
     * @return \Illuminate\Database\Eloquent\Collection
     */
    //private function collection($data);




}
