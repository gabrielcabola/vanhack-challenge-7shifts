<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use App\Repositories\LocationsRepository;
use App\Repositories\UsersRepository;

class LocationsController extends Controller
{


  public function __construct(LocationsRepository $locations) {
    $this->locations = $locations;
  }


  public function report() {
      $data['locations'] = $this->locations->get()->where('id','25753');
      return view('locations.list',$data);
  }

  public function detail($location_id) {


    $data['location'] = $this->locations->getDetail($location_id);
    $users = new UsersRepository($location_id);
    $data['users'] = $users->getUsers();

  

    return view('locations.detail',$data);
  }

}
