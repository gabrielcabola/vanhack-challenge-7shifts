<?php

namespace App\Repositories;




use Config;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use App\Repositories\DataRepository;
use App\Repositories\LocationRepository;


class LocationsRepository
{


    public $location;

    public function __construct() {
      $data = new DataRepository;
      $this->locations = $data->get('locations');
    }
    /**
     * {@inheritdoc}
     */
    public function get()
    {
        return $this->locations;
    }

    public function getDetail($location_id) {

        $repo = new LocationRepository($location_id);
        return $repo->detail();

    }





}
