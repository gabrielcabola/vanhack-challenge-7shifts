<?php

namespace App\Repositories;




use Config;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use App\Repositories\DataRepository;


class LocationRepository
{


    public $location_id;
    public $location;

    public function __construct($location_id) {

      $this->location_id = $location_id;

      $data = new DataRepository;
      $this->locations = $data->get('locations');
      $this->location = $this->detail();
    }


    /**
     * {@inheritdoc}
     */
    public function detail() {
        return $this->locations->where('id',$this->location_id)->first();
    }

    public function dailyOvertimeMultiplier() {
      return $this->location->labourSettings->dailyOvertimeMultiplier;
    }

    public function dailyOvertimeThreshold() {
      return $this->location->labourSettings->dailyOvertimeThreshold;;
    }

    public function overtime() {
      return $this->location->labourSettings->overtime;;
    }

    public function weeklyOvertimeMultiplier() {
      return $this->location->labourSettings->weeklyOvertimeMultiplier;;
    }

    public function weeklyOvertimeThreshold() {
      return $this->location->labourSettings->weeklyOvertimeThreshold;;
    }





}
