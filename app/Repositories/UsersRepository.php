<?php

namespace App\Repositories;

use Config;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use App\Repositories\DataRepository;
use App\Repositories\LocationRepository;
use App\Repositories\PunchsRepository;

class UsersRepository
{


    public $users;
    private $location_id;

    public function __construct($location_id) {
      $data = new DataRepository;
      $this->location_id = $location_id;
      //
      $this->location = new LocationRepository($location_id);
      $this->users = $data->get('users');
      //
      $timePunchs = new PunchsRepository();
      $this->timePunchs = $timePunchs->getTimes();

    }
    /**
     * {@inheritdoc}
     */
    public function getUsers()
    {

/**
 * Your job is to make a request to these end points and calculate the hours a user worked over all,
*  and broken down by location.
*   You must calculate the regular hours,
*   as well as daily
*   and weekly overtime hours.

*   Overtime rules come from a location’s setting.

*   Overtime is triggered one of two ways.

*    If an employee works more than X hours a day (daily overtime),
*     or more than Y hours a week (weekly overtime).
*
*     Overtime is paid for whichever overtime number is greater.

 */

        $list =  $this->users->flatten(1)->toArray()[0];
        $users = [];

        foreach ($list as $key => $user) {

          $calculatePunchs = $this->CalculatePunchs($this->timePunchs->where('userId',$user->id)->all());

          $user->daily_regular_hours = $this->regular_hours($calculatePunchs,'daily');
          $user->daily_overtime_hours = $this->overtime_hours($calculatePunchs,'daily');
          $user->weekly_regular_hours = $this->regular_hours($calculatePunchs,'weekly');
          $user->weekly_overtime_hours = $this->overtime_hours($calculatePunchs,'weekly');

          $user->finalWage = $this->finalWage($user);
          $user->finalHours = $this->finalHours($user);
          $user->finalOvertime = $this->finalOvertime($user);

          $users[$key] = $user;
        }

        return $users;

    }

    public function timePunchs($user_id) {

      //get timePunchs for a specifc user
      $timePunchs = new PunchsRepository($user_id);
      return $timePunchs->getTimes();

    }

    public function CalculatePunchs($timePunchs) {


          $daily = [];
          $weekly = [];
          $punchs = 0;


            foreach ($timePunchs as $key => $time) {

              //Calculate hours between Dates
              $startTime = Carbon::parse($time->clockedIn);
              $finishTime = Carbon::parse($time->clockedOut);
              $totalDuration = $finishTime->diffInMinutes($startTime); //In Minutes

              //Calculate how is extra or now extra defined by location set
              $overtime = ((int) $totalDuration - (int) $this->location->dailyOvertimeThreshold()); //here I can put some helpers
              //Location have overtime?
              if($overtime > 0) {

                  if($this->location->overtime()) {
                    $overtimeHour = $overtime/60;
                    $dailyWageOvertime = ($this->location->dailyOvertimeMultiplier() * $time->hourlyWage) * $overtimeHour;
                    //calculate overtime based on dailyOvertimeMultiplier
                    //calculate overtime based on weeklyOvertimeMultiplier
                  } else {
                    $dailyWageOvertime = 0;
                    //return the total numer of hours and price daily and weekly
                  }

              $dailyWage = (($totalDuration-$overtimeHour)/60) * $time->hourlyWage; //here I can put some helpers
              $dailyovertime = $dailyWage + $dailyWageOvertime;

              //RETURN DATA
                  $absoluteDate = date('y-m-d',strtotime($time->clockedIn)); // I Choose this because to group per date of start the shift //here I can put some helpers
                  $daily[$absoluteDate] = [
                        'min' => $this->increment($daily,$absoluteDate,'hours') + ($totalDuration-$overtimeHour),
                        'minOvertime' => $this->increment($daily,$absoluteDate,'overtime') + $overtime,
                        'wage' => $this->increment($daily,$absoluteDate,'wage') + $dailyWage,
                        'wageOvertime' => $this->increment($daily,$absoluteDate,'wageOvertime') + $dailyWageOvertime,
                        'punchs' => $this->increment($daily,$absoluteDate,'punchs') + 1,
                      ];

                //RETURN DATA
                  $weekAbsolute = Carbon::parse($time->clockedIn)->format('W');
                  $weekly[$weekAbsolute] = [
                    'min' => $this->increment($weekly,$weekAbsolute,'hours') + $totalDuration-$overtimeHour,
                    'minOvertime' => $this->increment($weekly,$weekAbsolute,'overtime') + $overtime,
                    'wage' => $this->increment($weekly,$weekAbsolute,'wage') + $dailyWage,
                    'wageOvertime' => $this->increment($weekly,$weekAbsolute,'wageOvertime') + $dailyWageOvertime,
                    'punchs' => $this->increment($daily,$absoluteDate,'punchs') + 1,
                  ];

                return [
                  'daily'=>$daily,
                  'weekly'=>$weekly
                ];

              }
          }
    }


    private function regular_hours($calc,$period) {
      $sumMinutes = 0;
      $sumWage = 0;
      $sumPunchs = 0;
      foreach ($calc[$period] as $key => $c) {
        $sumMinutes = $sumMinutes + $c['min'];
        $sumWage = $sumWage + $c['wage'];
        $sumPunchs = $sumPunchs + $c['punchs'];
      }
      return ['minutes'=>$sumMinutes,'wage'=>$sumWage,'punchs'=>$sumPunchs];
    }


    private function overtime_hours($calc,$period) {
      $sumMinutes = 0;
      $sumWage = 0;
        $sumPunchs = 0;
      foreach ($calc[$period] as $key => $c) {
        $sumMinutes = $sumMinutes + $c['minOvertime'];
        $sumWage = $sumWage + $c['wageOvertime'];
        $sumPunchs = $sumPunchs + $c['punchs'];
      }
      return ['minutes'=>$sumMinutes,'wage'=>$sumWage,'punchs'=>$sumPunchs];
    }


    private function increment($array,$index,$subindex) {
      if(! empty($array[$index])) {
        return $array[$index][$subindex];
      } else return 0;
    }

    private function finalWage($user) {
      if(
        ($user->daily_overtime_hours['wage']+$user->daily_regular_hours['wage']) > ($user->weekly_regular_hours['wage']+$user->weekly_overtime_hours['wage'])
        ) {
          return $user->daily_overtime_hours['wage']+$user->daily_regular_hours['wage'];
        } else {
          return $user->weekly_overtime_hours['wage']+$user->weekly_regular_hours['wage'];
        }
    }


    private function finalHours($user) {
    
      if($user->daily_regular_hours['minutes'] > $user->weekly_regular_hours['minutes']) {
        return $user->daily_regular_hours['minutes'];
      } else {
        return $user->weekly_regular_hours['minutes'];
      }
    }


    private function finalOvertime($user) {
      if($user->daily_overtime_hours['minutes'] > $user->weekly_overtime_hours['minutes']) {
        return $user->daily_overtime_hours['minutes'];
      } else {
        return $user->weekly_overtime_hours['minutes'];
      }
    }



}
