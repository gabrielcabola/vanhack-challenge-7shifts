<?php

namespace App\Repositories;


use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;
use Illuminate\Database\Eloquent\Collection;
use App\Contracts\Repositories\DataRepository as DataRepositoryContract;
//use Illuminate\Support\Facades\Cache;

class DataRepository implements DataRepositoryContract
{

    public $endpoints = [];

    public function __construct() {
      $this->endpoints = [
        'locations' => 'https://shiftstestapi.firebaseio.com/locations.json',
        'users' => 'https://shiftstestapi.firebaseio.com/users.json',
        'timePunchs' => 'https://shiftstestapi.firebaseio.com/timePunches.json'
      ];

      // $this->endpoints = [
      //   'locations' => asset('data/location.json'),
      //   'users' => asset('data/users.json'),
      //   'timePunchs' => asset('data/timePunches.json'),
      // ];

    }
    /**
     * {@inheritdoc}
     */
    public function get($endpoint)
    {

      //if($endpointCache = Cache::get($endpoint)) return $endpointCache;
      $client = new Client();
      try {
          $response = $client->request('get', $this->GetUrl($endpoint));
          $response = $response->getBody()->getContents();
        //  $endpointCache = Cache::put($endpoint,$this->collection($response));
          $endpointCache = $this->collection($response);
          return $endpointCache;
        }
        catch (GuzzleHttp\Exception\ClientException $e) {
          $response = $e->getResponse();
          $responseBodyAsString = $response->getBody()->getContents();
        }

    }


    /**
     * {@inheritdoc}
     */
    public function collection($data)
    {
      return collect(json_decode($data));
      //  return Gallery::where('slug',$slug)->get()->first();
    }


    private function getUrl($endpoint) {
      $url = $this->endpoints[$endpoint];
      if(! empty($url)) { return $url; }  else return 'no url found';
    }




}
